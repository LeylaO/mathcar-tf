﻿# PRIMER HITO DEL TRABAJO FINAL

### Curso: Introducción a los Algoritmos
### Profesor: Canaval Sanchez, Luis Martin
### Grupo 05 - Miembros : 
**Gallardo Morales, Carla Alejandra**
**Ortiz Laura, Leyla Alisson**
**Philco Mota, Katty Yolanda**
### Sección: SW1H
### Ciclo: 2024-01

## 1. Introducción:
**Contexto:** Ausencia del proyecto de un videojuego que se desarrollará empleando los temas aprendidos en la clase de Introducción a los Algoritmos, y desprotección del Estado a los niños de bajos recursos para alcanzar una educación satisfactoria.

**Problemática:** Debido al desinterés del gobierno peruano en la educación primaria de niños de bajos recursos económicos, este grupo presenta dificultades para resolver ejercicios matemáticos. Por ello, mediante el videojuego a realizar, apuntamos a enseñar una pequeña porción de matemáticas: fracciones y decimales, y ecuaciones de suma y resta.

**Motivación:** En primer lugar, ayudar a niños de bajos recursos en el nivel de educación primaria a desenvolverse satisfactoriamente en dos temas matemáticos, porque teniendo esta porción de conocimiento comprenderán actividades de la vida real fundamentales, por ejemplo, economía. Del mismo modo, lograrán crecer profesionalmente, y aplicarán lo aprendido en su vida cotidiana.  En segundo lugar, nuestra motivación es crecer como programadoras al poner en práctica lo aprendido en el curso, demostrando así nuestras capacidades.

**Propuesta de solución:** Nuestra propuesta de solución es implementar un juego de matemáticas con los temas: fracciones y decimales, y ecuaciones de suma y resta, enfocado en niños de bajos recursos económicos en el nivel de educación primaria.

**Objetivo:** Desarrollar un videojuego en modo consola en lenguaje C++, para enseñar fracciones y decimales, y ecuaciones de suma y resta, a niños de nivel primaria de  bajos recursos económicos, que carecen de este tipo de conocimientos.

**Metodología:** La metodología a implementar para la elaboración del videojuego es, gestionar datos, diseñar escenarios/mundos, definir las acciones del personaje principal y enemigos. Asimismo, planificar  el desarrollo del juego, determinar las opciones que debe  tener el menú principal y sus funcionalidades al elegir una opción, y lo que ocurre si se solicita al programa una opción que no pertenece a las alternativas, es decir, las acciones y sus respuestas. Por otro lado, decidir la información que poseerá la ventana principal (puntos, vidas, etc.) y acordar la repartición de actividades:
MIEMBRO | ACTIVIDAD A REALIZAR PARA EL DESARROLLLO DEL VIDEOJUEGO | 
-- | -- |
Gallardo Morales, Carla Alejandra |Header file del escenario 1, funcion que muestra el escenario, que actualiza la posición del carro y enemigos, y la función del puntaje.
Ortiz Laura, Leyla Alisson | Header file del escenario 2, función que imprime el carro (personaje principal), función que imprime y valida las operaciones con decimales, y la del conteo de vida.
Philco Mota, Katty Yolanda | Header file del escenario 3, función que imprime y valida una ecuación aleatoria, funciones para imprimir el menú (printxy, printFrame, menuPrincipal).

 Del mismo modo para la elaboración del videojuego, nos reuniremos presencialmente o virtualmente, al terminar nuestras clases. Finalmente, mediante una secuencia de pruebas, comprobaremos la funcionalidad de nuestro videojuego.

### CONCLUSIONES
Para este primer hito, hemos logrado poner en práctica satisfactoriamente lo aprendido en el curso, incluso lo que no estaba previsto desarrollar aún (matrices en C++). Por ello, consideramos que vamos por buen camino para cumplir nuestro objetivo principal.


## 2. Desarrollo

### Requisitos funcionales
ID | Requisito | 
-- | -- |
R001 | Implementar una pantalla del menú principal
R002 | Implementar una pantalla con las instrucciones del videojuego
R003 | Implementar una pantalla con los créditos del videojuego
R004 | Implementar tres pantallas con tres mundos distintos
R005 | Implementar funciones para la validación de las operaciones con decimales y fracciones
R006|Implementar funciones para la validación de las operaciones de ecuaciones
R007|Implementar una ventana con los mensajes de los enemigos
R008|Desarrollar funciones para el movimiento del personaje principal, enemigos y tutor
R009| Desarrollar funciones para el conteo de vidas y puntaje
R010| Implementar pantalla de ganador de partida
R011 | Desarrollar función para salir de la consola del videojuego

### Diagrama de módulos de programación
en word
|  |  |
|--|--|
|  |  |


### HEADER FILE PRE-ELIMINAR DE LAS FUNCIONES
```c++  
#pragma once
#include  <iostream>
#include  <iomanip>
#include  "upc.h"
using  namespace std;
using uint = unsigned  int;
void  showEscenario() {
 //muestra el escenario
}
void  updateCar(int, int, char) {
 //actualizará la posicion del carro de acuerdo a la direccion que mueva el jugador
}
void  printCar(int ncol, int nrow, int& col, int& row, Colors color) {
 //imprimirá el carro
}
void  updateEnemies(int, int, int) {
 //actualizará automaticamente la posicion de los enemigos
}
void  mensajeTutor() {
}
void  mensajeEnemigo() {
}
bool  ecuacion() {
 //imprime un ecuacion aleatoria y de acuerdo a la respuesta del jugador retorna falso o verdadero
}
bool  decimales() {
 //imprime una operación con decimales aleatoria y de acuerdo a la respuesta del jugador retorna falso o verdadero
}
int  countVida(bool) {
 //contará las vidas del jugador
}
//funciones para imprimir el menu
void  printxy(uint x, int y, string txt) {
 gotoxy(x, y);
 cout << txt;
}
void  printFrame(uint x, uint y, uint width, uint height) {
}
int  menuPrincipal(uint left, uint top, uint width, uint height) {
}
}  
```
**URL DEL REPOSITORIO**
https://gitlab.com/LeylaO/mathcar-tf
