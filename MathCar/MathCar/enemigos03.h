
#include "pch.h"
#include <iomanip>
#include "upc.h"
#include "escenario3.h"


using namespace std;

int trafficlight[MAXROWS][MAXCOLS] =
{
    {0, 0, 1, 1, 1, 1, 0, 0},
    {0, 0, 1, 2, 2, 1, 0, 0},
    {0, 1, 1, 3, 3, 1, 1, 0},
    {0, 1, 1, 4, 4, 1, 1, 0},
    {0, 0, 1, 1, 1, 1, 0, 0},
    {0, 0, 1, 1, 1, 1, 0, 0},
    {0, 0, 0, 1, 1, 0, 0, 0},
    {0, 0, 0, 1, 1, 0, 0, 0},
    {0, 0, 0, 1, 1, 0, 0, 0}
};

int trafficcone[MAXROWS][MAXCOLS] =
{
  
    {0, 0, 0, 0, 1, 0, 0, 0, 0},
    {0, 0, 0, 2, 2, 2, 0, 0, 0},
    {0, 0, 1, 1, 1, 1, 1, 0, 0},
    {0, 2, 2, 2, 2, 2, 2, 2, 0},
    {1, 1, 1, 1, 1, 1, 1, 1, 1},

};

//int policeman[MAXROWS][MAXCOLS] =
//{
//
//
//
//
//
//
//
//};



void showtrafficlight() {
    foreground(BLACK);
    for (int i = 0; i < MAXROWS; ++i) {
        for (int j = 0; j < MAXCOLS; ++j) {
            if (trafficlight[i][j] == 1) {
                gotoxy(j + 57, i + 15);
                cout << BLOCK;
            }
        }
    }
    foreground(DARK_RED);
    for (int i = 0; i < MAXROWS; ++i) {
        for (int j = 0; j < MAXCOLS; ++j) {
            if (trafficlight[i][j] == 2) {
                gotoxy(j + 57, i + 15);
                cout << BLOCK;
            }
        }
    }
    foreground(DARK_YELLOW);
    for (int i = 0; i < MAXROWS; ++i) {
        for (int j = 0; j < MAXCOLS; ++j) {
            if (trafficlight[i][j] == 3) {
                gotoxy(j + 57, i + 15);
                cout << BLOCK;
            
            }
        }
    }
    foreground(BRIGHT_GREEN);
    for (int i = 0; i < MAXROWS; ++i) {
        for (int j = 0; j < MAXCOLS; ++j) {
            if (trafficlight[i][j] == 4) {
                gotoxy(j + 57, i + 15);
                cout << BLOCK;
            }
        }
    }
}
void showtrafficcone() {
    foreground(BRIGHT_RED);
    for (int i = 0; i < MAXROWS; ++i) {
        for (int j = 0; j < MAXCOLS; ++j) {
            if (trafficcone[i][j] == 1) {
                gotoxy(j + 50, i + 0);
                cout << BLOCK;
              
            }
        }
    }
    foreground(WHITE);
    for (int i = 0; i < MAXROWS; ++i) {
        for (int j = 0; j < MAXCOLS; ++j) {
            if (trafficcone[i][j] == 2) {
                gotoxy(j + 50, i + 0);
                cout << BLOCK;
            }
        }
    }

}




int main()
{
   
    hideCursor();
    showLaberinto();
    showarbol();
    showTambo();
    showChifa();
    showUniversidad();
    showtrafficlight();
    showtrafficcone();

    
    system("pause>0");
    return 0;
}
