#ifndef __FUNCIONES_V2_H__
#define __FUNCIONES_V2_H__
#include <iostream>
#include <iomanip>
#include <random>
#include <sstream>
#include <limits>
#include "const_mc.h"
#include "escenario1.h"
#include "escenario2_v2.h"
#include "escenario3_v2.h"
#include "enemigos1.h"
#include "enemigos02.h"
#include "presentacion.h"
#include "enemywindow.h"
#include "gameover.h"
#include "upc.h"

using namespace std;
using uint = unsigned int;

int totalMaps = 0;

//variables globales para mapa2
int vida1;
int opportunity1;

//variables globales para mapa2
int vida2;
int opportunity2;

//variables globales para mapa3
int vida3;
int opportunity3;

void initGlobal() {
    vida1 = 3;
    opportunity1 = 0;
    vida2 = 3;
    opportunity2 = 0;
    vida3 = 3;
    opportunity3 = 0;
}

int randint(int min, int max, random_device& rd) {
    mt19937_64 gen(rd());
    return gen() % (max - min) + min;
}

double randDouble(int min, int max, int cifras) {
    random_device device;
    mt19937 generador(device());
    uniform_real_distribution<> distribucion(min, max);
    stringstream ss;
    ss << fixed << setprecision(cifras) << distribucion(generador);
    return std::stod(ss.str());
}

struct Usuario {
    string *name = new string[10];
    int* puntaje = new int[10];
};

void printCar(int ncol, int nrow, int& col, int& row, Colors color) {
    gotoxy(col, row);
    row = nrow;
    col = ncol;
    gotoxy(col, row);
    foreground(color);
    cout << "  ____";
    gotoxy(col, row + 1);
    foreground(color);
    cout << " /_||_\\\_";
    gotoxy(col, row + 2);
    foreground(color);
    cout << "( _   _ \\";
    gotoxy(col, row + 3);
    foreground(color);
    cout << " (_)-(_)";
}

void printenemy(Colors color) {
    gotoxy(11, 9);
    foreground(color);
    cout << "   ^";
    gotoxy(11, 10);
    foreground(color);
    cout << "  /|\\";
    gotoxy(11, 11);
    foreground(color);
    cout << " / | \\";
    gotoxy(11, 12);
    foreground(color);
    cout << "/  |  \\";
}

void deleteCar(int col, int row) {
    gotoxy(col, row);
    cout << "         ";
    gotoxy(col, row + 1);
    cout << "         ";
    gotoxy(col, row + 2);
    cout << "         ";
    gotoxy(col, row + 3);
    cout << "         ";
}

void deleteEjercicio() {
    gotoxy(60, 10);
    cout << "                                ";
    gotoxy(60, 12);
    cout << "                                ";
}

void deleteEjercicio1() {
    gotoxy(1, 17);
    cout << "                            ";
    gotoxy(1, 18);
    cout << "                            ";
    gotoxy(1, 19);
    cout << "                            ";
}

void deleteenemy3() {
    gotoxy(11, 9);
    cout << "         ";
    gotoxy(11, 10);
    cout << "         ";
    gotoxy(11, 11);
    cout << "         ";
    gotoxy(11, 12);
    cout << "         ";
}

void countLifes(int& vida, bool respuesta, bool& gameover) {
    foreground(DARK_RED);
    if (!respuesta) {
        vida -= 1;
        gotoxy(60, 1);
        cout << "      ";
    }
    if (vida == 0) {
        gameover = true;
        showgameover();
       //cout << "El usuario " << usuario << "perdi� en el mapa" << totalMaps;
    }
    gotoxy(60, 1);
    for (int i = 0; i < vida; ++i) {
        cout << "<3";
    }
}

void printRayitas2() { //rayas de la pista - escenario 2 
    foreground(DARK_YELLOW);
    for (int i = 0; i < MAXROWS; ++i) {
        for (int j = 0; j < MAXCOLS; ++j) {
            if (map2[i][j] == 2) {
                gotoxy(j, i);
                cout << bordeH;
            }
            if (map2[i][j] == 3) {
                gotoxy(j, i);
                cout << bordeV;
            }
            if (map2[i][j] == 4) {
                gotoxy(j, i);
                cout << interseccion;
            }
            if (map2[i][j] == 5) {
                gotoxy(j, i);
                cout << interseccion2;
            }
            if (map2[i][j] == 6) {
                gotoxy(j, i);
                cout << esquina;
            }
        }
    }
}

double* numDecim() {
    int n = 3;
    double* decimales = new double[n];
    for (int i = 0; i < n; ++i) {
        decimales[i] = randDouble(0.1, 99.0, 3);
    }
    return decimales;
}

bool decimales(int nivel, rataa& rata, Cat& gato) {
    double* decimal = numDecim();
    int correcto = 0;
    double respuesta;

    correcto += decimal[1]*1000;
    correcto += decimal[2]*1000;

    gotoxy(62, 10);
    foreground(WHITE);
    #undef max
    cout << nivel << ". " << decimal[1] << " + " << decimal[2] << " = ";
    if (cin.fail()) {
        cin.clear();
    }
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

    cin >> respuesta;
    cout << endl;
    
    if (respuesta*1000 == correcto) {
        gotoxy(62, 12);
        cout << "Correcto!\n";
        switch (nivel) {
        case 1:
            rata.estado = true;
            deleteEnemigo2();
            break;
        case 2:
            int* erow = new int; *erow = 23;
            int* ecol = new int; *ecol = 105;
            gato.estado = true;
            for (int i = 0; i < 105; ++i) {
                updateGato(ecol, erow, gato);
            }
            deleteEnemy(0, 23);
            printRayitas2();
            sleep4(80);
            break;
        }
        return true;
    }
    else {
        gotoxy(62, 12);
        cout << "Respuesta incorrecta";
        return false;
    }
}
void printRayitas3() { //rayas de la pista - escenario 3
    foreground(BLACK);
    for (int i = 0; i < MAXROWS; ++i) {
        for (int j = 0; j < MAXCOLS; ++j) {
            if (laberinto[i][j] == 3) {
                gotoxy(j, i);
                cout << BLOCK;
            }
        }
    }
    foreground(DARK_YELLOW);
    for (int i = 0; i < MAXROWS; ++i) {
        for (int j = 0; j < MAXCOLS; ++j) {
            if (laberinto[i][j] == 4) {
                gotoxy(j, i);
                cout << BLOCK;
            }
        }
    }
}
bool ecuacion(int nivel, conitoo& conito, Cono& cono) {
    random_device rd;

    double correcto = 0;
    double respuesta;
    double variable = randint(2, 6, rd);
    double num = randint(20, 101, rd);
    double total = randint(50, 200, rd);

    background(BLACK);
    foreground(BRIGHT_GREEN);
    gotoxy(60, 10);
    cout << nivel << ". " << variable << "x" << "+" << num << "=" << total << ", x = ";
    if (cin.fail()) {
        cin.clear();
    }
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

    cin >> respuesta;
    cout << endl;
    correcto = (total - num) / variable;
    stringstream ss;
    ss << fixed << setprecision(2) << correcto;
    ss >> correcto;

    if (respuesta == correcto) {
        gotoxy(62, 12);
        cout << "Correcto!\n";
        switch (nivel) {
        case 1:
            conito.estado = true;
            deleteEnemigo3();
            break;
        case 2:
            int* erow = new int; *erow = 23;
            int* ecol = new int; *ecol = 9;
            cono.estado = true;
            for (int i = 0; i < 9; ++i) {
                updateCono(ecol, erow, cono);
            }
            deleteEnemy(0, 23);
            break;
        }
        return true;
    }
    else {
        gotoxy(62, 12);
        cout << "Respuesta incorrecta";
        return false;
    }
}

bool fraccion(int nivel, renoo& renooo, pinguinm& pinguinmm) {
    random_device rd;
    double correcto = 0;
    double respuesta;
    double num1; double num2;
    int a = randint(1, 5, rd);
    int b = randint(1, 5, rd);
    int c = randint(2, 6, rd);
    int d = randint(2, 6, rd);

    background(BLACK);
    foreground(BRIGHT_BLACK);
    gotoxy(1, 17);
    cout << nivel << ".  " << a << "  +  " << b << endl;
    gotoxy(1, 18);
    cout << "   " << "---" << "   ---" << "  =" << endl;
    gotoxy(1, 19);
    cout << "    " << c << "    " << d << endl;
    gotoxy(19, 18);
    if (cin.fail()) {
        cin.clear();
    }
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    cin >> respuesta;
    cout << endl;

    num1 = (a * d) + (c * b);
    num2 = c * d;
    correcto = num1 / num2;

    stringstream ss;
    ss << fixed << setprecision(2) << correcto;
    ss >> correcto; 

    if (respuesta == correcto) {
        gotoxy(1, 17);
        cout << "Correcto!";
        switch (nivel){
            case 1 :
                renooo.estado = true;
                deleteReno();
                break;
            case 2: 
     
                pinguinmm.estado = true;
                deletePinguin();
                break;
         }
        return true;
    }
    else {
        gotoxy(1, 17);
        cout << "Respuesta incorrecta";
        return false;
    }
}

void printRayitas1() { //rayas de la pista - escenario 1
    foreground(BLACK);
    for (int i = 0; i < MAXROWS; ++i) {
        for (int j = 0; j < MAXCOLS; ++j) {
            if (maze[i][j] == 3) {
                gotoxy(j, i);
                cout << BLOCK;
            }
        }
    }

    foreground(BRIGHT_YELLOW);
    for (int i = 0; i < MAXROWS; ++i) {
        for (int j = 0; j < MAXCOLS; ++j) {
            if (maze[i][j] == 2) {
                gotoxy(j, i);
                cout << BLOCK;
            }
        }
    }
    foreground(BLACK);
    for (int i = 0; i < MAXROWS; ++i) {
        for (int j = 0; j < MAXCOLS; ++j) {
            if (maze[i][j] == 1) {
                gotoxy(j, i);
                cout << BLOCK;
            }
        }
    }
}

bool updateCar1(int*& row, int*& col, bool& gameover, renoo& renooo, pinguinm& pinguinmm, char key = 0) {
    int nrow = *row, ncol = *col;

    switch (key) {
    case 'W':--nrow;
        break;
    case 'A': --ncol;
        break;
    case 'S': ++nrow;
        break;
    case 'D': ++ncol;
        break;
    }

    if (nrow < 0 || nrow >= MAXROWS || ncol < 0 || ncol >= MAXCOLS) {
        return false;
    }
    if (maze[nrow][ncol] != 1 && maze[nrow][ncol] != 2 && maze[nrow][ncol] != 3) {
        return false;
    }
    if (maze[nrow + 3][ncol] != 1 && maze[nrow + 3][ncol] != 2 && maze[nrow + 3][ncol] != 3) {
        return false;
    }
    if (maze[nrow][ncol + 8] != 1 && maze[nrow][ncol + 8] != 2 && maze[nrow][ncol + 8] != 3) {
        return false;
    }
    if (maze[nrow + 3][ncol + 8] != 1 && maze[nrow + 3][ncol + 8] != 2 && maze[nrow + 3][ncol + 8] != 3) {
        return false;
    }

    printRayitas1();
    deleteEjercicio1();
    deleteCar(*col, *row);
    printCar(ncol, nrow, *col, *row, DARK_RED);
    showportal1();
    countLifes(vida1, true, gameover);

    bool respuesta;
    if (*row == 16 && *col == 37) {
        respuesta = fraccion(1, renooo, pinguinmm);
        countLifes(vida1, respuesta, gameover);
    }
    else if (*row == 23 && *col == 72) {
        respuesta = fraccion(2, renooo, pinguinmm);
        countLifes(vida1, respuesta, gameover);
    }
    else if (*row == 23 && *col == 100) {
        respuesta = fraccion(3, renooo, pinguinmm);
        countLifes(vida1, respuesta, gameover);
        return respuesta;

    }
    return false;
}

//para mapa 2
bool updateCar2(int*& row, int*& col, bool& gameover, rataa& rata, Cat& gato, char key = 0) {
    int nrow = *row, ncol = *col;
    switch (key) {
    case 'W':--nrow;
        break;
    case 'A': --ncol;
        break;
    case 'S': ++nrow;
        break;
    case 'D': ++ncol;
        break;
    }

    if (nrow < 0 || nrow >= MAXROWS || ncol < 0 || ncol >= MAXCOLS) {
        return false;
    }
    if (map2[nrow][ncol] != 1 && map2[nrow][ncol] != 2 && map2[nrow][ncol] != 3
        && map2[nrow][ncol] != 4 && map2[nrow][ncol] != 5 && map2[nrow][ncol] != 6) {
        return false;
    }
    if (map2[nrow + 3][ncol] != 1 && map2[nrow + 3][ncol] != 2 && map2[nrow + 3][ncol] != 3
        && map2[nrow + 3][ncol] != 4 && map2[nrow + 3][ncol] != 5 && map2[nrow + 3][ncol] != 6) {
        return false;
    }
    if (map2[nrow][ncol + 8] != 1 && map2[nrow][ncol + 8] != 2 && map2[nrow][ncol + 8] != 3
        && map2[nrow][ncol + 8] != 4 && map2[nrow][ncol + 8] != 5 && map2[nrow][ncol + 8] != 6) {
        return false;
    }
    if (map2[nrow + 3][ncol + 8] != 1 && map2[nrow + 3][ncol + 8] != 2 && map2[nrow + 3][ncol + 8] != 3
        && map2[nrow + 3][ncol + 8] != 4 && map2[nrow + 3][ncol + 8] != 5 && map2[nrow + 3][ncol + 8] != 6) {
        return false;
    }
    printRayitas2();
    deleteEjercicio();
    deleteCar(*col, *row);
    printCar(ncol, nrow, *col, *row, DARK_CYAN);
    countLifes(vida2, true, gameover);
    showportal();
    bool respuesta;
    if (*row == 6 && *col == 32) {
        if (opportunity2 == 0) {
            respuesta = decimales(1, rata, gato);
            countLifes(vida2, respuesta, gameover);
            opportunity2 += 1;
        }
    }
    else if (*row == 20 && *col == 104) {
        if (opportunity2 == 1) {
            respuesta = decimales(2, rata, gato);
            countLifes(vida2, respuesta, gameover);
            opportunity2 += 1;
        }
    }
    else if ((*row == 23 || *row == 24) && *col == 4) {
        respuesta = decimales(3, rata, gato);
        countLifes(vida2, respuesta, gameover);
        return respuesta;
    }
    return false;
}

//para mapa 3
bool updateCar3(int*& row, int*& col, bool& gameover, conitoo& conito, Cono& cono, char key = 0) {
    int nrow = *row, ncol = *col;
    switch (key) {
    case 'W':--nrow;
        break;
    case 'A': --ncol;
        break;
    case 'S': ++nrow;
        break;
    case 'D': ++ncol;
        break;
    }
    if (nrow < 0 || nrow >= MAXROWS || ncol < 0 || ncol >= MAXCOLS) {
        return false;
    }
    if (laberinto[nrow][ncol] != 3 && laberinto[nrow][ncol] != 4) {
        return false;
    }
    if (laberinto[nrow + 3][ncol] != 3 && laberinto[nrow + 3][ncol] != 4) {
        return false;
    }
    if (laberinto[nrow][ncol + 8] != 3 && laberinto[nrow][ncol + 8] != 4) {
        return false;
    }
    if (laberinto[nrow + 3][ncol + 8] != 3 && laberinto[nrow + 3][ncol + 8] != 4) {
        return false;
    }
    printRayitas3();
    deleteEjercicio();
    deleteCar(*col, *row);
    printCar(ncol, nrow, *col, *row, DARK_CYAN);
    countLifes(vida3, true, gameover);
    
    bool respuesta;
    if ((*row == 9 || *row == 8) && *col == 2) {
        if (opportunity3 == 0) {
            respuesta = ecuacion(1, conito, cono);
            countLifes(vida3, respuesta, gameover);
            opportunity3 += 1;
        }
    }
    else if ((*row == 18 || *row == 19) && *col == 8) {
        if (opportunity3 == 1) {
            respuesta = ecuacion(2, conito, cono);
            countLifes(vida3, respuesta, gameover);
            opportunity3 += 1;
        }
    }
    else if ((*row == 23 || *row == 24) && *col == 100) {
        respuesta = ecuacion(3, conito, cono);
        countLifes(vida3, respuesta, gameover);
        return respuesta;
    }
    return false;
}

//funciones para imprimir el menu y pantalla principal
void printxy(uint x, int y, string txt) {
    gotoxy(x, y);
    cout << txt;
}

int menuPrincipal() {
    int op;
    do {
        clear();
        showPresentacion();
        cin >> op;
    } while (op < 1 || op > 4);
    return op;
}

void rankingWin(char usuario) {
    cout << "El usuario " << usuario << " gan� el juego con " << vida1 + vida2 + vida3 << " vidas."
        << "Logro pasar los 3 mapas";
}

#endif