#pragma once
#include <iostream>
#include "upc.h"
#include "tutorwindow.h"
#include "tutorwindow1.h"
#include "creditos.h"
#include "funciones_v2.h"
#include "enemigos02.h"
#include "winner.h"

using namespace std;

bool GOmap1;
bool GOmap2;
bool GOmap3;

void initGo() {
    GOmap1 = false;
    GOmap2 = false;
    GOmap3 = false;
}

void game1(bool& gameover, int*& col, int*& row, int*& erow, int*& ecol, renoo& renooo, pinguinm& pinguinmm) {
    
    if (renooo.estado == false) {
        showReno(renooo);
    }
    if (pinguinmm.estado == false) {
        showPinguin(pinguinmm);
    }

    if (_kbhit()) {
        char key = toupper(_getch());
        int teclado = 0;
        switch (key) {
        case 27: // ESC
            GOmap1 = true;
            break;
        case 32: // Barra de espacio
            showmatriztutor1();
            while (teclado != 32) {
                teclado = toupper(_getch());
            }
            showmaze();
            showescenario1();
            _getch();
            break;
        case 'W':
        case 'A':
        case 'S':
        case 'D':
            if (updateCar1(row, col, gameover, renooo, pinguinmm, key)) { //cambiar por la funcion de tu mapa
                GOmap1 = true;
            }
        }
    }
    totalMaps += 1;
    sleep4(50);
}

void game2(bool& gameover, int*& row, int*& col, int*& erow, int*& ecol, Cat& miGato, rataa& miRata) {

    if (miRata.estado == false) {
        showEnemigo2(miRata);
    }
    if (miGato.estado == false) {
        designGato(ecol, erow, 5, miGato);
    }

    if (_kbhit()) {
        char key = toupper(_getch());
        int teclado = 0;
        switch (key) {
        case 27: // ESC
            GOmap2 = true;
            break;
        case 32: // Barra de espacio
            showtutor2();
            while (teclado != 32) {
                teclado = toupper(_getch());
            }
            printPista();
            showMap2();
            _getch();
            break;
        case 'W':
        case 'A':
        case 'S':
        case 'D':
            if (updateCar2(row, col, gameover, miRata, miGato, key)) {
                GOmap2 = true;
            }
        }
    }
    sleep4(50);
}

void game3(bool& gameover, int*& row, int*& col, int*& erow, int*& ecol, Cono& miCono, conitoo& miConito) {
    if (miConito.estado == false) {
        showenemigo3(miConito);
    }
    if (miCono.estado == false) {
        designCono(ecol, erow, 5, miCono);
    }
    if (_kbhit()) {
        char key = toupper(_getch());
        int teclado = 0;
        switch (key) {
        case 27: // ESC
            GOmap3 = true;
            break;
        case 32: // Barra de espacio
            showmatriztutor2();
            while (teclado != 32) {
                teclado = toupper(_getch());
            }
            showLaberinto();
            _getch();
            break;
        case 'W':
        case 'A':
        case 'S':
        case 'D':

            if (updateCar3(row, col, gameover, miConito, miCono, key)) {
                GOmap3 = true;
                showwinner();
                _getch();
            }
        }
    }
    totalMaps += 1;
    sleep4(50);
    gameover = true;
}

void gameplay() {
    bool gameover = false;

    int* row = new int; *row = 1;
    int* col = new int; *col = 7;
    int* erow = new int;
    int* ecol = new int;
 
    renoo renooo; pinguinm pinguinmm;
    for (int i = 0; i < 5; ++i) {
        for (int j = 0; j < 12; ++j) {
            renooo.matriz[i][j] = reno[i][j];
        }
    }
    for (int i = 0; i < 6; ++i) {
        for (int j = 0; j < 8; ++j) {
            pinguinmm.matriz[i][j] = pinguinmagenta[i][j];
        }
    }
    hideCursor();
    showmaze();
    showescenario1();
    updateCar1(row, col, gameover, renooo, pinguinmm);

    while (!gameover) {
        while (!GOmap1) {
            game1(gameover, col, row, erow, ecol, renooo, pinguinmm);
        }

        *row = 2;
        *col = 1;
        *erow = 23;
        *ecol = 105;
        rataa miRata;
        for (int i = 0; i < 3; ++i) {
            for (int j = 0; j < 8; ++j) {
                miRata.matriz[i][j] = rata[i][j];
            }
        }
        Cat miGato;
        hideCursor();
        showMap2();
        printPista();
        updateCar2(row, col, gameover, miRata, miGato);
        while (!GOmap2) {
            game2(gameover, row, col, erow, ecol, miGato, miRata);
        }
        *row = 9;
        *col = 1;
        *erow = 23;
        *ecol = 9;
        conitoo miConito;
        for (int i = 0; i < 5; ++i) {
            for (int j = 0; j < 9; ++j) {
                miConito.matriz[i][j] = conito[i][j];
            }
        }
        Cono miCono;
        hideCursor();
        showLaberinto();
        updateCar3(row, col, gameover, miConito, miCono);
        while (!GOmap3) {
            game3(gameover, row, col, erow, ecol, miCono, miConito);
        }
    }
    delete row;
    delete col;
    delete erow;
    delete ecol;
}