#include <iostream>
#include "upc.h"
#include "instrucciones.h"
#include "creditos.h"
#include "funciones_v2.h"
#include "enemigos02.h"
#include "winner.h"
#include "playGame.h"

using namespace std;

int main() { 

    hideCursor();
    int opcion;
    int n = 0;
    do {
        opcion = menuPrincipal();
        switch (opcion) {
        case 1:
            clear();
            showmatrizins();
            showmatrizcar();
            _getch();
            break;
        case 2:
            clear();
            showMatrizCred();
            _getch();
            break;
        while (n < 11) {
        case 3:
            Usuario us;
            bool gameover = false;

            int opc;
            do {
                initGlobal();
                initGo();
                int typeUser;
                cout << "�Qu� tipo de usuario eres?\n";
                cout << "1. Usuario nuevo\n"
                    << "2. Usuario existente\n"
                    << "3. Salir\n";

                if (cin.fail()) {
                    cin.clear();
                }
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                cin >> typeUser;
                cout << endl;
                
                string nombre = us.name[n];
                if (typeUser == 1) {
                    
                    cout << "Ingrese su nombre de usuario: ";
                    if (cin.fail()) {
                        cin.clear();
                    }
                    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                    cin >> nombre;
                    cout << endl;
                }
                else if (typeUser == 2) {
                    cout << "Ingrese su n�mero de usuario: ";
                    if (cin.fail()) {
                        cin.clear();
                    }
                    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                    cin >> n;
                    cout << endl;
                    cout << "Hola " << us.name[n];

                    _getch();
                }
                else if (typeUser==3) {
                    opcion = 4;
                }

                gameplay();

                gotoxy(0, 0);
                cout << "Usted fue el usuario " << nombre << " de n�mero: " << n << "\n";
                cout << "1.Intentar otra vez\n";
                cout << "2. Salir\n";
                cout << "Escoja una opci�n: ";
                if (cin.fail()) {
                    cin.clear();
                }
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                cin >> opc;

                us.puntaje[n] = vida1 + vida2 + vida3;
                n += 1;
                initGlobal();
                initGo();
                resetAll();
                
            }while (opc!=2);
        }
        }
    } while (opcion != 4);

    //return opcion;

    return EXIT_SUCCESS;
}