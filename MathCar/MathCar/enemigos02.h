#pragma once
#include <iomanip>
#include <random>
#include <sstream>
#include <string>
#include "upc.h"
#include "escenario2_v2.h"
#include "const_mc.h"

using namespace std;

void printObj(int col, int row, string obj[], int numColor) {
    for (int i = 0; i < obj->size(); ++i) {
        for (int j = 0; j < obj[i].size(); j++) {
            if (obj[i][j] != ' ') {
                gotoxy(col + j, row + i);
                foreground(numColor);
                cout << obj[i][j];
            }
        }
    }
}

struct Cat {
    bool estado = false; //false=quieto    true=byebye
    string gatou[8] = { " /\\    /", "(0 )  (", "(  \\   )", "|(__) /" };
};

struct Cono {
    bool estado = false; //false=quieto true=chau
    string conou[8] = { "   ^", "  /|\\", " / | \\", "/  |  \\" };
};

void designGato(int* col, int* row, int numColor, Cat& gato) {
    foreground(numColor);
    printObj(*col, *row, gato.gatou, 5);
}

void designCono(int* col, int* row, int numColor, Cono& cono) {
    foreground(numColor);
    printObj(*col, *row, cono.conou, 5);
}

void deleteEnemy(int col, int row) {
    gotoxy(col, row);
    cout << "         ";
    gotoxy(col, row + 1);
    cout << "         ";
    gotoxy(col, row + 2);
    cout << "         ";
    gotoxy(col, row + 3);
    cout << "         ";
}

void updateGato(int* col, int* row, Cat& gato) {
    *col += -1;
    gotoxy(*col, *row);
    deleteEnemy(*col, *row);
    designGato(col, row, 5, gato);
}

void updateCono(int* col, int* row, Cono& cono) {
    *col += -1;
    gotoxy(*col, *row);
    deleteEnemy(*col, *row);
    designCono(col, row, 5, cono);
}

int rata[3][8] = {
    {21, 33, 21, 0, 0, 21, 33, 21},
    {21, 21, 0, 21, 21, 0, 21, 21},
    {0, 21, 21, 23, 23, 21, 21, 0}
};

int conito[5][9] = {
{0 ,0 ,0 ,0 ,9 ,0 ,0 ,0 ,0},
{0 ,0 ,0 ,9 ,2 ,9 ,0 ,0 ,0},
{0 ,0 ,9 ,2 ,9 ,2 ,9 ,0 ,0},
{0 ,9 ,2 ,9 ,2 ,9 ,2 ,9 ,0},
{9, 2, 9, 2, 9, 2, 9, 2, 9}
};

int portal[5][10] = {
{0 ,0 ,0 ,1 ,1 ,1 ,3 ,1 ,2 ,0},
{0 ,0 ,2 ,3 ,3 ,1 ,3 ,3 ,1 ,2},
{0 ,2 ,1 ,1 ,3 ,1 ,0 ,0 ,1 ,2},
{0 ,2 ,1 ,3 ,1 ,3 ,3 ,1 ,2 ,2},
{0 ,0 ,1 ,3 ,3 ,1 ,1 ,2 ,0 ,0}
};

int reno[5][12] = {
    {2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2},
    {0, 0, 2, 2, 2, 1, 1, 2, 2, 2, 0, 0},
    {0, 0, 2, 7, 6, 1, 1, 6, 7, 2, 0, 0 },
    {0, 0, 2, 2, 2, 1, 1, 2, 2, 2, 0, 0},
    {0, 0, 2, 2, 2, 5, 5, 2, 2, 2, 0, 0 },

};

int pinguinmagenta[6][8] = {
    {1, 1, 1, 1, 1, 1, 1, 1 },
    {1, 3, 3, 3, 3, 3, 3, 1 },
    {1, 3, 4, 3, 3, 4, 3, 1 },
    {1, 1, 3, 2, 2, 3, 1, 1 },
    {1, 3, 3, 3, 3, 3, 3, 1 },
    {1, 1, 2, 1, 1, 2, 1, 1 },

};

struct renoo {
    bool estado = false; //verdadero=muerto    falso=no muerto
    int matriz[5][12];
};

struct pinguinm {
    bool estado = false; //verdadero = muerto falso = no muerto
    int matriz[6][8];
};

void showReno(renoo& reno) {
    foreground(DARK_RED);
    for (int i = 0; i < 5; ++i) {
        for (int j = 0; j < 12; ++j) {
            if (reno.matriz[i][j] == 1) {
                gotoxy(j + 35, i + 22);
                cout << BLOCK;
            }
        }
    }

    foreground(BRIGHT_RED);
    for (int i = 0; i < 5; ++i) {
        for (int j = 0; j < 12; ++j) {
            if (reno.matriz[i][j] == 2) {
                gotoxy(j + 35, i + 22);
                cout << BLOCK;
            }
        }
    }
    foreground(BRIGHT_RED);
    for (int i = 0; i < 5; ++i) {
        for (int j = 0; j < 12; ++j) {
            if (reno.matriz[i][j] == 3) {
                gotoxy(j + 35, i + 22);
                cout << BLOCK;
            }
        }
    }

    foreground(DARK_WHITE);
    for (int i = 0; i < 5; ++i) {
        for (int j = 0; j < 12; ++j) {
            if (reno.matriz[i][j] == 4) {
                gotoxy(j + 35, i + 22);
                cout << BLOCK;
            }
        }
    }
    foreground(BLACK);
    for (int i = 0; i < 5; ++i) {
        for (int j = 0; j < 12; ++j) {
            if (reno.matriz[i][j] == 5) {
                gotoxy(j + 35, i + 22);
                cout << BLOCK;
            }
        }
    }
    foreground(BRIGHT_CYAN);
    for (int i = 0; i < 5; ++i) {
        for (int j = 0; j < 12; ++j) {
            if (reno.matriz[i][j] == 6) {
                gotoxy(j + 35, i + 22);
                cout << BLOCK;
            }
        }
    }
    foreground(WHITE);
    for (int i = 0; i < 5; ++i) {
        for (int j = 0; j < 12; ++j) {
            if (reno.matriz[i][j] == 7) {
                gotoxy(j + 35, i + 22);
                cout << BLOCK;
            }
        }
    }
}


void showPinguin(pinguinm& pinguinmagenta) {

    foreground(DARK_WHITE);
    for (int i = 0; i < 6; ++i) {
        for (int j = 0; j < 8; ++j) {
            if (pinguinmagenta.matriz[i][j] == 3) {
                gotoxy(j + 82, i + 22);
                cout << BLOCK;
            }
        }
    }
    foreground(BRIGHT_MAGENTA);
    for (int i = 0; i < 6; ++i) {
        for (int j = 0; j < 8; ++j) {
            if (pinguinmagenta.matriz[i][j] == 1) {
                gotoxy(j + 82, i + 22);
                cout << BLOCK;
            }
        }
    }
    foreground(DARK_YELLOW);
    for (int i = 0; i < 6; ++i) {
        for (int j = 0; j < 8; ++j) {
            if (pinguinmagenta.matriz[i][j] == 2) {
                gotoxy(j + 82, i + 22);
                cout << BLOCK;
            }
        }
    }
    foreground(BLACK);
    for (int i = 0; i < 6; ++i) {
        for (int j = 0; j < 8; ++j) {
            if (pinguinmagenta.matriz[i][j] == 4) {
                gotoxy(j + 82, i + 22);
                cout << BLOCK;
            }
        }
    }
}

void deleteReno() {
    for (int i = 0; i < 5; ++i) {
        for (int j = 0; j < 12; ++j) {
            if (reno[i][j] != 0) {
                gotoxy(j + 35, i + 22);
                cout << " ";
            }
        }
    }
}

void deletePinguin() {
    for (int i = 0; i < 6; ++i) {
        for (int j = 0; j < 8; ++j) {
            if (pinguinmagenta[i][j] != 0) {
                gotoxy(j + 82, i + 22);
                cout << " ";
            }
        }
    }
}

void showportal() {
    foreground(BLACK);
    for (int i = 0; i < 5; ++i) {
        for (int j = 0; j < 10; ++j) {
            if (portal[i][j] == 0) {
                gotoxy(j + 1, i + 23);
                cout << FINAL;
            }
        }
    }
    foreground(DARK_MAGENTA);
    for (int i = 0; i < 5; ++i) {
        for (int j = 0; j < 10; ++j) {
            if (portal[i][j] == 1) {
                gotoxy(j + 1, i + 23);
                cout << BLOCK;
            }
        }
    }
    foreground(BRIGHT_BLUE);
    for (int i = 0; i < 5; ++i) {
        for (int j = 0; j < 10; ++j) {
            if (portal[i][j] == 2) {
                gotoxy(j + 1, i + 23);
                cout << FINAL;
            }
        }
    }
    foreground(BRIGHT_MAGENTA);
    for (int i = 0; i < 5; ++i) {
        for (int j = 0; j < 10; ++j) {
            if (portal[i][j] == 3) {
                gotoxy(j + 1, i + 23);
                cout << BLOCK;
            }
        }
    }
}

void showportal1() {
    foreground(BLACK);
    for (int i = 0; i < 5; ++i) {
        for (int j = 0; j < 10; ++j) {
            if (portal[i][j] == 0) {
                gotoxy(j + 108, i + 23);
                cout << FINAL;
            }
        }
    }
    foreground(DARK_MAGENTA);
    for (int i = 0; i < 5; ++i) {
        for (int j = 0; j < 10; ++j) {
            if (portal[i][j] == 1) {
                gotoxy(j + 108, i + 23);
                cout << BLOCK;
            }
        }
    }
    foreground(BRIGHT_BLUE);
    for (int i = 0; i < 5; ++i) {
        for (int j = 0; j < 10; ++j) {
            if (portal[i][j] == 2) {
                gotoxy(j + 108, i + 23);
                cout << FINAL;
            }
        }
    }
    foreground(BRIGHT_MAGENTA);
    for (int i = 0; i < 5; ++i) {
        for (int j = 0; j < 10; ++j) {
            if (portal[i][j] == 3) {
                gotoxy(j + 108, i + 23);
                cout << BLOCK;
            }
        }
    }
}

struct rataa {
    bool estado = false; //verdadero=muerto    falso=no muerto
    int matriz[3][8];
};

struct conitoo {
    bool estado = false; //verdadero = muerto falso = no muerto
    int matriz[5][9];
};

void showEnemigo2(rataa& rata) {
    foreground(BRIGHT_BLACK);
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 8; ++j) {
            if (rata.matriz[i][j] == 21) {
                gotoxy(j + 33, i + 9);
                cout << BLOCK;
            }
        }
    }
    background(BRIGHT_BLACK);
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 8; ++j) {
            if (rata.matriz[i][j] == 33) {
                gotoxy(j + 33, i + 9);
                cout << arbusto;
            }
            if (rata.matriz[i][j] == 23) {
                gotoxy(j + 33, i + 9);
                cout << "-";
            }
        }
    }
    foreground(BRIGHT_MAGENTA);
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 8; ++j) {
            if (rata.matriz[i][j] == 33) {
                gotoxy(j + 33, i + 9);
                cout << arbusto;
            }
        }
    }
}

void showenemigo3(conitoo& conito) {
    foreground(BRIGHT_BLACK);
    for (int i = 0; i < 5; ++i) {
        for (int j = 0; j < 9; ++j) {
            if (conito.matriz[i][j] == 2) {
                gotoxy(j + 9, i + 9);
                cout << FINAL;
            }
        }
    }
    foreground(DARK_RED);
    for (int i = 0; i < 5; ++i) {
        for (int j = 0; j < 9; ++j) {
            if (conito.matriz[i][j] == 9) {
                gotoxy(j + 9, i + 9);
                cout << BLOCK;
            }
        }
    }
}

void deleteEnemigo2() {
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 8; ++j) {
            if (rata[i][j] != 0) {
                gotoxy(j + 33, i + 9);
                cout << " ";
            }
        }
    }
}

void deleteEnemigo3() {
    for (int i = 0; i < 5; ++i) {
        for (int j = 0; j < 9; ++j) {
            if (conito[i][j] != 0) {
                gotoxy(j + 9, i + 9);
                cout << " ";
            }
        }
    }
}
