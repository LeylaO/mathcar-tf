#ifndef __CONST_MC_H__
#define __CONST_MC_H__

#include <iostream>
#include "upc.h"

using namespace std;

using uint = unsigned int;
const uint32_t MAXROWS = 30;
const uint32_t MAXCOLS = 120;
const uint8_t BLOCK = '\xdb';
const uint8_t TEXTURA = '\xb2';
const uint8_t FINAL = 176;

#endif
